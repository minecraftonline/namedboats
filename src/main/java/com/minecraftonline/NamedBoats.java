package com.minecraftonline;

import java.awt.Event;
import java.util.HashSet;
import java.util.Optional;

import org.slf4j.Logger;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.entity.hanging.Painting;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.entity.projectile.DamagingProjectile;
import org.spongepowered.api.entity.projectile.EyeOfEnder;
import org.spongepowered.api.entity.projectile.Firework;
import org.spongepowered.api.entity.projectile.source.ProjectileSource;
import org.spongepowered.api.entity.vehicle.Boat;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.damage.DamageModifierTypes;
import org.spongepowered.api.event.cause.entity.damage.DamageTypes;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.type.Exclude;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.equipment.EquipmentType;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.google.common.collect.Sets;
import com.google.inject.Inject;

@Plugin(
        id = "namedboats",
        name = "NamedBoats",
        authors = { "Anna_28" },
        description = "A plugin that allows naming of boat, armor stand, item frame, and painting items to name the entity created. And breaking the named entity returning the named item."
)
public class NamedBoats
{
    @Inject
    private Logger logger;
    private HashSet<ItemType> boatTypes = Sets.newHashSet(ItemTypes.BOAT, ItemTypes.SPRUCE_BOAT, ItemTypes.BIRCH_BOAT, ItemTypes.JUNGLE_BOAT, ItemTypes.ACACIA_BOAT, ItemTypes.DARK_OAK_BOAT);
    private HashSet<EquipmentType> standSlots = Sets.newHashSet(EquipmentTypes.BOOTS, EquipmentTypes.LEGGINGS, EquipmentTypes.CHESTPLATE, EquipmentTypes.HEADWEAR, EquipmentTypes.MAIN_HAND, EquipmentTypes.OFF_HAND);

    @Listener
    public void onServerStart(GameStartedServerEvent event)
    {
        logger.info("NamedBoats Started");
    }

    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    @Listener
    public void onSpawnNamedEntity(SpawnEntityEvent event)
    {
        Optional<ItemStackSnapshot> usedItemOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if (!usedItemOptional.isPresent()) return;
        ItemStackSnapshot usedItem = usedItemOptional.get();

        Optional<EntityType> entityType = Optional.empty();
        if (boatTypes.contains(usedItem.getType()))
        {
            entityType = Optional.of(EntityTypes.BOAT);
        }
        else if (usedItem.getType() == ItemTypes.ARMOR_STAND)
        {
            entityType = Optional.of(EntityTypes.ARMOR_STAND);
        }
        else if (usedItem.getType() == ItemTypes.PAINTING)
        {
            entityType = Optional.of(EntityTypes.PAINTING);
        }
        else if (usedItem.getType() == ItemTypes.ITEM_FRAME)
        {
            entityType = Optional.of(EntityTypes.ITEM_FRAME);
        }
        else if (usedItem.getType() == ItemTypes.END_CRYSTAL)
        {
            entityType = Optional.of(EntityTypes.ENDER_CRYSTAL);
        }
        else if (usedItem.getType() == ItemTypes.FIREWORKS)
        {
            entityType = Optional.of(EntityTypes.FIREWORK);
        }
        else if (usedItem.getType() == ItemTypes.SNOWBALL)
        {
            entityType = Optional.of(EntityTypes.SNOWBALL);
        }
        else if (usedItem.getType() == ItemTypes.SPLASH_POTION || usedItem.getType() == ItemTypes.LINGERING_POTION)
        {
            entityType = Optional.of(EntityTypes.SPLASH_POTION);
        }
        else if (usedItem.getType() == ItemTypes.ENDER_EYE)
        {
            entityType = Optional.of(EntityTypes.EYE_OF_ENDER);
        }
        else if (usedItem.getType() == ItemTypes.ENDER_PEARL)
        {
            entityType = Optional.of(EntityTypes.ENDER_PEARL);
        }
        else
        {
            // no supported item found
            return;
        }

        if (entityType.isPresent())
        {
            for (Entity entity : event.getEntities())
            {
                if (entity.getType() == entityType.get())
                {
                    usedItem.get(Keys.DISPLAY_NAME).ifPresent(text -> entity.offer(Keys.DISPLAY_NAME, text));
                }
            }
        }
    }

    @Listener
    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    public void onSpawnEntityEvent(SpawnEntityEvent event)
    {
        Optional<EyeOfEnder> eyeOfEnder = event.getCause().first(EyeOfEnder.class);
        if (!eyeOfEnder.isPresent()) return;

        Optional<Text> name = eyeOfEnder.get().get(Keys.DISPLAY_NAME);
        if (!name.isPresent()) return;

        for (int i = 0; i < event.getEntities().size(); i++)
        {
            Entity entity = event.getEntities().get(i);
            if (!(entity instanceof Item)) return;
            Item itemEntity = (Item) entity;
            Optional<ItemStackSnapshot> optionalItem = itemEntity.get(Keys.REPRESENTED_ITEM);
            if (!optionalItem.isPresent())
            {
                // this really shouldn't be possible
                return;
            }
            ItemStackSnapshot item = optionalItem.get();
            // only rename item entities that are the broken entity
            if (item.getType() == ItemTypes.ENDER_EYE)
            {
                if (item.get(Keys.DISPLAY_NAME).isPresent())
                {
                    // this item is already named, don't change the name
                    continue;
                }

                itemEntity.offer(Keys.REPRESENTED_ITEM, item.with(Keys.DISPLAY_NAME, name.get()).get());

                // only rename first one
                break;
            }
        }
    }

    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    @Listener
    public void onDropNamedEntityItem(DropItemEvent.Pre event)
    {
        // add name to broken boat item
        Optional<Boat> boat = event.getCause().first(Boat.class);
        Optional<Painting> painting = event.getCause().first(Painting.class);
        Optional<ItemFrame> itemframe = event.getCause().first(ItemFrame.class);
        Entity entity;
        HashSet<ItemType> itemTypes = new HashSet<>();

        if (boat.isPresent())
        {
            entity = boat.get();
            itemTypes = boatTypes;
        }
        else if (painting.isPresent())
        {
            entity = painting.get();
            itemTypes.add(ItemTypes.PAINTING);
        }
        else if (itemframe.isPresent())
        {
            entity = itemframe.get();
            itemTypes.add(ItemTypes.ITEM_FRAME);
        }
        else
        {
            // no supported named entity found
            return;
        }

        Optional<Text> name = entity.get(Keys.DISPLAY_NAME);
        if (itemTypes.isEmpty() || !name.isPresent()) return;

        // doesn't work with boats
        if (entity.getType() != EntityTypes.BOAT && !entity.isRemoved())
        {
            // entity hasn't been removed, could be item frame in item frame
            return;
        }

        for (int i = 0; i < event.getDroppedItems().size(); i++)
        {
            ItemStackSnapshot item = event.getDroppedItems().get(i);
            // only rename item entities that are the broken entity
            if (itemTypes.contains(item.getType()))
            {
                if (item.get(Keys.DISPLAY_NAME).isPresent())
                {
                    // this item is already named, don't change the name
                    continue;
                }

                event.getDroppedItems().set(i, item.with(Keys.DISPLAY_NAME, name.get()).get());
                // only rename first one
                break;
            }
        }
    }

    @Listener
    public void onDestroyNamedEntity(DestructEntityEvent event, @First DamagingProjectile projectile)
    {
        ProjectileSource source = projectile.getShooter();
        if (source instanceof Player)
        {
            onDestroyNamedEntity(event, (Player) source);
        }
    }

    @Listener
    public void onDestroyNamedEntity(DestructEntityEvent event, @First Player player)
    {
        // drop boat item if named boat is broken in creative mode
        // this is to match minecarts doing the same thing

        if (player.gameMode().get() == GameModes.CREATIVE)
        {
            Entity target = event.getTargetEntity();

            Optional<Text> name = target.get(Keys.DISPLAY_NAME);
            if (!name.isPresent()) return;

            Optional<ItemStack> itemStack = Optional.empty();

            if (target instanceof Boat)
            {
                Optional<TreeType> type = target.get(Keys.TREE_TYPE);
                if (!type.isPresent()) return;

                ItemStack item;
                if (type.get() == TreeTypes.OAK)
                {
                    item = ItemStack.of(ItemTypes.BOAT);
                }
                else if (type.get() == TreeTypes.SPRUCE)
                {
                    item = ItemStack.of(ItemTypes.SPRUCE_BOAT);
                }
                else if (type.get() == TreeTypes.BIRCH)
                {
                    item = ItemStack.of(ItemTypes.BIRCH_BOAT);
                }
                else if (type.get() == TreeTypes.JUNGLE)
                {
                    item = ItemStack.of(ItemTypes.JUNGLE_BOAT);
                }
                else if (type.get() == TreeTypes.ACACIA)
                {
                    item = ItemStack.of(ItemTypes.ACACIA_BOAT);
                }
                else if (type.get() == TreeTypes.DARK_OAK)
                {
                    item = ItemStack.of(ItemTypes.DARK_OAK_BOAT);
                }
                else
                {
                    // unknown boat type
                    logger.error("Unknown boat type: " + type.get().toString());
                    return;
                }
                itemStack = Optional.of(item);
            }
            else if (target instanceof Painting)
            {
                itemStack = Optional.of(ItemStack.of(ItemTypes.PAINTING));
            }
            else if (target instanceof ItemFrame)
            {
                itemStack = Optional.of(ItemStack.of(ItemTypes.ITEM_FRAME));
            }
            else
            {
                // no supported named entity found
                return;
            }

            if (itemStack.isPresent())
            {
                ItemStack item = itemStack.get();
                item.offer(Keys.DISPLAY_NAME, name.get());

                Location<World> location = target.getLocation();

                Item itemEntity = (Item) location.createEntity(EntityTypes.ITEM);
                itemEntity.offer(Keys.REPRESENTED_ITEM, item.createSnapshot());

                location.spawnEntity(itemEntity);
            }
        }
    }

    @Listener
    public void onDestroyArmorStand(DamageEntityEvent event)
    {
        if (!event.willCauseDeath()) return;
        if (event.isCancelled()) return;

        if (event.getTargetEntity() instanceof ArmorStand)
        {
            ArmorStand stand = (ArmorStand) event.getTargetEntity();

            Optional<Text> name = stand.get(Keys.DISPLAY_NAME);
            if (!name.isPresent()) return;

            if (event.getSource() != null && event.getSource() instanceof DamageSource) {
                DamageSource source = (DamageSource) event.getSource();
                // don't drop items from void damage (including Vanilla /kill command)
                if (source.getType() == DamageTypes.VOID) return;
            }

            // cancel this to prevent usual item drop
            // doing this because can't seem to get the event for item drop
            // *and* be able to tell it was from an armor stand
            event.setCancelled(true);

            Location<World> location = stand.getLocation();

            HashSet<ItemStack> items = Sets.newHashSet();

            // get the items on the stand to drop
            for (EquipmentType type: standSlots)
            {
                Optional<ItemStack> equipment = stand.getEquipped(type);
                if (equipment.isPresent())
                {
                    items.add(equipment.get());
                }
            }


            // but still kill the stand
            stand.remove();

            // make the armor stand item
            ItemStack standItem = ItemStack.of(ItemTypes.ARMOR_STAND);
            standItem.offer(Keys.DISPLAY_NAME, name.get());
            items.add(standItem);

            for (ItemStack item: items)
            {
                Item itemEntity = (Item) location.createEntity(EntityTypes.ITEM);
                itemEntity.offer(Keys.REPRESENTED_ITEM, item.createSnapshot());

                location.spawnEntity(itemEntity);
            }
        }
    }

    @Listener
    public void onNameTagArmorStand(InteractEntityEvent event, @First Player player)
    {
        // deny name tag use on armor stands
        if (!(event.getTargetEntity() instanceof ArmorStand)) return;

        Optional<ItemStackSnapshot> usedItemOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if (!usedItemOptional.isPresent()) return;
        ItemStackSnapshot usedItem = usedItemOptional.get();
        if (usedItem.getType() != ItemTypes.NAME_TAG) return;

        event.setCancelled(true);
    }

    @Listener
    public void onExplosionDamage(DamageEntityEvent event, @First Firework firework)
    {
        // prevent damage from invulnerable fireworks
        Optional<Boolean> invulnerable = firework.get(Keys.INVULNERABLE);
        if (invulnerable.isPresent() && invulnerable.get())
        {
            event.setCancelled(true);
        }
    }
}
